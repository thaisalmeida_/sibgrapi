#-*- coding:utf-8  -*-
from django.conf.urls import patterns, url,handler404,handler500
from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'lcmu/', include('sibigrapiapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/$','django.contrib.auth.views.logout_then_login', {'login_url': 'lcmu/'}),
)

handler404 = 'sibigrapigapp.views.pagina_404'
handler500 = 'sibigrapigapp.views.pagina_500'