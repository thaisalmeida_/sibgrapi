#-*- coding:utf-8 -*-
from pymongo import MongoClient
from pymongo import ReturnDocument
import sys

def geraNroTweetSerial(colecao):
	""" Controi campo serial  """
	#resultado = colecao.find_one()
	indice = 1
	resultado = colecao.find_one_and_update({'nro_tweet': 0}, {'$inc': {'nro_tweet': indice}})
	
	while resultado != None:
		indice +=1
		resultado = colecao.find_one_and_update({'nro_tweet': 0}, {'$inc': {'nro_tweet': indice}})


def verificaNroTweetSerial(colecao):
	indice = 0
	while indice < 100500:
		resultado = colecao.find({'nro_tweets': indice})
		print indice,resultado.count()
		indice += 1

#def geraIntervalosTweets(colecao):
	#total = 100500
	#nro_intervalos = 15
	#limite_inferior = 0

	#for i in range(0,15):
		
	#	limite_superior = limite_inferior+6700
	#	result = colecao.update_many({'nro_tweet':{'$gt': limite_inferior,'$lt':limite_superior+1}},
	#	 {'$set':{'intervalo':[limite_inferior+1,limite_superior] }})
	#	limite_inferior = limite_superior

def geraIntervalosTweets(colecao):
	#total = 100500
	#nro_intervalos = 15
	limite_inferior = 0

	for i in range(0,6):		
		limite_superior = limite_inferior+1000
		result = colecao.update_many({'nro_tweet':{'$gt': limite_inferior,'$lt':limite_superior+1}},
		 {'$set':{'intervalo':[limite_inferior+1,limite_superior] }})
		limite_inferior = limite_superior



def geraRotuladoresPorIntervaloTweets(colecao,lista):

	total = 100500
	nro_intervalos = 15
	limite_inferior = 0

	for i in range(0,len(lista)):
		limite_superior = limite_inferior+1000
		result = colecao.update_many({'nro_tweet':{'$gt': limite_inferior,'$lt':limite_superior+1}},
		 {'$set':{'nome_rotulador':lista[i]}})
		limite_inferior = limite_superior

def exibeResultados(resultado):
	count = 0
	for i in resultado:
		print resultado[i]

def estabaleceConexao():
	try:
		cliente = MongoClient()
	except: 
		print "Não foi possível estabelcer a conexão..."
		cliente = None
	return cliente

def executaScript():
	
	#Lista de combinacoes 6, 4 a 4
	rotuladores = ['Thais','Bruno','Lucas','Gilvan','Leandro','Paulo']

	combinacoes = [['Thais','Bruno','Lucas','Gilvan'],['Thais','Bruno','Lucas','Leandro'],['Thais','Bruno','Lucas','Paulo'],
	['Thais','Bruno','Gilvan','Leandro'],['Thais','Bruno','Gilvan','Paulo'],['Thais','Bruno','Leandro','Paulo'],
	['Thais','Lucas','Gilvan','Leandro'],['Thais','Lucas','Gilvan','Paulo'],['Thais','Lucas','Paulo','Leandro'],
	['Gilvan','Lucas','Leandro','Paulo'],['Gilvan','Lucas','Bruno','Leandro'],['Paulo','Lucas','Bruno','Leandro'],
	['Leandro','Bruno','Gilvan','Paulo'],['Paulo','Bruno','Gilvan','Lucas'],['Leandro','Thais','Gilvan','Paulo']]	

	#Estabelece conexao
	cliente = estabaleceConexao()
	basededados = cliente['sibigrapidb']
	colecao = basededados['sibigrapiapp_tweetsnaorotulados']

	#operacoes
	geraNroTweetSerial(colecao)
	geraIntervalosTweets(colecao)
	geraRotuladoresPorIntervaloTweets(colecao,rotuladores)


if __name__ == "__main__":
	executaScript()