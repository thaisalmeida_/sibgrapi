from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.user_login, name='user_login'),
    url(r'listagem/$', views.lista_rotulos, name='lista_rotulos'),
    url(r'sobre/', views.sobre, name='sobre'),
    url(r'^(?P<nro_tweet>[0-9]+)/rotulagem/$', views.rotulagem, name='rotulagem'),
    url(r'^(?P<nro_tweet>[0-9]+)/editar_rotulo/$', views.editar_rotulo, name='editar_rotulo'),
]
