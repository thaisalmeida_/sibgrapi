#-*- coding:utf-8  -*-
from django.shortcuts import render,render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from models import TweetsNaoRotulados
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login
from django_mongodb_engine.contrib import MongoDBManager
from django.core.urlresolvers import reverse
from django.template import Context, loader,RequestContext

def user_login(request):
	if request.method == 'POST':
		username = request.POST.get('login')
		password = request.POST.get('senha')

		user = authenticate(username=username, password=password)

		if user is not None:
			login(request,user)
			return HttpResponseRedirect('listagem/') 
		else:
			
			return render(request,'sibigrapiapp/login.html',{'mensagem_erro':'Digite um login e senha válidos...'})
	else:
		return render(request,'sibigrapiapp/login.html',{})

def sobre(request):
	return render(request,'sibigrapiapp/sobre.html',{})

def pagina_404(request):
	#primeira forma
	# template = loader.get_template('sibigrapiapp/500.html')
	# context = Context({
	# 	'mensagem_erro':'A url não foi encontrada. Tente http://www.rotulaew.com/lcmu/' 
	# 	})

	# return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=404)
	response = render_to_response('sibigrapiapp/404.html', {},context_instance=RequestContext(request))
	response.status_code = 404
	return response



def pagina_500(request):
	#primeira forma
	# template = loader.get_template('sibigrapiapp/404.html')
	# context = Context({
	# 	'mensagem_erro':'A url não foi encontrada. Tente http://www.rotulaew.com/lcmu/' 
	# 	})

	# return HttpResponse(content=template.render(context), content_type='text/html; charset=utf-8', status=500)
	response = render_to_response('sibigrapiapp/404.html', {},context_instance=RequestContext(request))
	response.status_code = 500
	return response

def editar_rotulo(request,nro_tweet):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/lcmu/')
	else:
		mensagem_erro = None
		mensagem_sucesso = None
		opniao = None
		rotulador = defineRotulador(request.user.username)
		consulta = {'nro_tweet':int(nro_tweet),'nome_rotulador':rotulador}
		
		if request.method == 'POST':
			if request.POST.get('options') is None:
				mensagem_erro = 'Escolha um dos rótulos abaixo'							
			else:
				atualizaRotulo(rotulador,int(request.POST.get('options')),consulta)
				mensagem_sucesso = 'Rótulo alterado com sucesso!Clique no menu "rotular" para continuar rotulando os tweets ou clique no menu "listagem"'
		
		resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:1] 
		
		if (resultado is None or len(resultado)==0 ):
			return HttpResponseRedirect('listagem/')

		if (int(resultado[0].opinioes[0][u'negativo']) == 1): 
			opniao = 2 #negativo
		elif int(resultado[0].opinioes[0][u'positivo'] == 1):
			opniao = 1 #positivo
		else:
			return HttpResponseRedirect('/lcmu/listagem/')#nao foi rotulado



		return render(request,'sibigrapiapp/editar_rotulo.html',
			{'tweets':resultado,
			'nro_tweet':int(resultado[0].nro_tweet),
			'mensagem_erro':mensagem_erro,
			'mensagem_sucesso':mensagem_sucesso,
			'opniao':opniao
			}
			)

def rotulagem(request,nro_tweet):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/lcmu/')	
	else:
		mensagem = None
		rotulador = defineRotulador(request.user.username)
		consulta = {'nro_tweet':int(nro_tweet),'nome_rotulador':rotulador}

		if request.method == 'POST':
			if request.POST.get('options') is None:
				mensagem = 'Escolha um dos rótulos abaixo'							
			else:
				adicionaRotulo(rotulador,int(request.POST.get('options')),consulta)

				consulta_nrotweet = {'nome_rotulador':rotulador, 'opinioes':{'$elemMatch':{'positivo':0,'negativo':0}}}
				resultado_nrotweet = TweetsNaoRotulados.objects.raw_query(consulta_nrotweet)[:1]

				if (resultado_nrotweet is None or len(resultado_nrotweet)==0 ):
					return HttpResponseRedirect('/lcmu/listagem/')

				return HttpResponseRedirect(reverse('rotulagem',args=(int(resultado_nrotweet[0].nro_tweet),)))
		

		resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:1]		
		
		if ((resultado is None or len(resultado)==0) or (verificaTweetJaRotulado(rotulador,int(resultado[0].nro_tweet)) is True)):
			return HttpResponseRedirect('/lcmu/listagem/')

		
		return render(request,'sibigrapiapp/rotulagem.html',{'tweets':resultado,'nro_tweet':int(resultado[0].nro_tweet),'mensagem_erro':mensagem})


def lista_rotulos(request):
	if not request.user.is_authenticated():
		return HttpResponseRedirect('/lcmu/')	
	else:
		rotulador = defineRotulador(request.user.username)
		mensagem_erro = None
		#Caso o menu 'rotular' seja ativado, define tweet a ser rotulado
		consulta_nrotweet = {'nome_rotulador':rotulador, 'opinioes':{'$elemMatch':{'positivo':0,'negativo':0}}}
		resultado_nrotweet = TweetsNaoRotulados.objects.raw_query(consulta_nrotweet)[:1]

		print 'resultado_nrotweet',resultado_nrotweet

		if request.method == 'POST':
			if ((request.POST.get('id') is None) or (len(request.POST.get('id'))<=0)):
				mensagem_erro = 'Dígite um id válido!'	
				return render(request,'sibigrapiapp/listagem_rotulos.html',{'tweets':[],'nro_tweet':int(resultado_nrotweet[0].nro_tweet),'mensagem_erro':mensagem_erro})						
			else:
				consulta = {'nro_tweet':int(request.POST.get('id')),'nome_rotulador':rotulador,'opinioes':{'$elemMatch':{'$or':[{'positivo':{'$gt':0},'negativo':0},{'positivo':0,'negativo':{'$gt':0}}]}}}
				resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:1]
				tweets = limpaDadosConsulta(resultado,rotulador)
				if (tweets is None or len(tweets)==0 ):
					mensagem_erro = 'Não houve resultado para a sua busca. O tweet procurado pode não existir ou não pertencer aos seus intervalos de rotulação...'

				return render(request,'sibigrapiapp/listagem_rotulos.html',{'tweets':tweets,'nro_tweet':int(resultado_nrotweet[0].nro_tweet),'mensagem_erro':mensagem_erro})
		else:

			consulta = {'nome_rotulador':rotulador,'opinioes':{'$elemMatch':{'$or':[{'positivo':{'$gt':0},'negativo':0},{'positivo':0,'negativo':{'$gt':0}}]}}}
			resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:20]
			tweets = limpaDadosConsulta(resultado,resultado)

			if (tweets is None or len(tweets)==0 ):
				mensagem_erro = 'Você ainda não rotulou nenhum tweet! Para começar, clique no menu "rotular"'

			if (resultado_nrotweet is None or len(resultado_nrotweet)==0 ):
				mensagem_erro = 'Parabéns, você terminou de rotular os tweets!Obrigada pela sua colaboração!'
				return render(request,'sibigrapiapp/listagem_rotulos.html',{'tweets':tweets,'nro_tweet':0,'mensagem_erro':mensagem_erro})

			return render(request,'sibigrapiapp/listagem_rotulos.html',{'tweets':tweets,'nro_tweet':int(resultado_nrotweet[0].nro_tweet),'mensagem_erro':mensagem_erro})

####################### Métodos Auxiliares #####################################

def limpaDadosConsulta(resultado,rotulador):
	lista = []
	rotulo = ''
	descricao = ''
	for i in resultado:	
		if len(i.descricao)>=60:
			descricao = i.descricao[:60]
		else:
			descricao = i.descricao

		if (int(i.opinioes[0][u'negativo'])==1):
			lista.append({'autor':i.autor,'descricao':descricao,'nro_tweet':int(i.nro_tweet),'data_hora_tweet':i.data_hora_tweet,'rotulo':'Negativo'})
		else:
			lista.append({'autor':i.autor,'descricao':descricao,'nro_tweet':int(i.nro_tweet),'data_hora_tweet':i.data_hora_tweet,'rotulo':'Positivo'})
	return lista

def defineRotulador(username):
	rotulador = ''
	if username.startswith('Thais'.lower()):
		rotulador = 'Thais'
	elif username.startswith('Bruno'.lower()):
		rotulador = 'Bruno'
	elif username.startswith('Lucas'.lower()):
		rotulador = 'Lucas'
	elif username.startswith('Gilvan'.lower()):
		rotulador = 'Gilvan'
	elif username.startswith('Leandro'.lower()):
		rotulador = 'Leandro'	
	elif username.startswith('Paulo'.lower()):
		rotulador = 'Paulo'		
	return rotulador

def adicionaRotulo(rotulador,opniao,consulta):
	resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:1]
	opnioes = resultado[0].opinioes
	if (opniao == 1):
		opnioes[0][u'positivo'] +=1 
	elif (opniao == 2):
		opnioes[0][u'negativo'] +=1
	TweetsNaoRotulados.objects.raw_update(consulta, {'$set':{'opinioes':opnioes}})



def atualizaRotulo(rotulador,opniao,consulta):
	resultado = TweetsNaoRotulados.objects.raw_query(consulta)[:1]
	opnioes = resultado[0].opinioes
	opniao_antiga = None

	
	#somente ha atualizacao se houver mudanca de opniao
	if (int(opnioes[0][u'positivo']) == 1):
		opniao_antiga = 1
	elif (int(opnioes[0][u'negativo']) == 1):
		opniao_antiga = 2


	print opniao,opniao_antiga
	if (opniao != opniao_antiga):
		#a contagem do rotulo anterior e decrementada
		if (opniao_antiga == 1):
			opnioes[0][u'positivo'] -=1 
		elif (opniao_antiga == 2):
			opnioes[0][u'negativo'] -=1

		#dados atualizado
		if (opniao == 1):
			opnioes[0][u'positivo'] +=1 
		elif (opniao == 2):
			opnioes[0][u'negativo'] +=1

		TweetsNaoRotulados.objects.raw_update(consulta, {'$set':{'opinioes':opnioes}}) 


def verificaTweetJaRotulado(rotulador,nro_tweet):
	rotulado = False
	consulta = {'nro_tweet':nro_tweet,'opinioes':{'$elemMatch':{'$or':[{'positivo':{'$gt':0},'negativo':0},{'positivo':0,'negativo':{'$gt':0}}]}}}
	resultado = TweetsNaoRotulados.objects.raw_query(consulta)
	if len(resultado) > 0:
		rotulado = True
	return rotulado