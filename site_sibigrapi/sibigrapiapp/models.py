#-*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django_mongodb_engine.contrib import MongoDBManager
from djangotoolbox.fields import ListField
from djangotoolbox.fields import EmbeddedModelField

class TweetsNaoRotulados(models.Model):
	nro_tweet = models.IntegerField()
	autor = models.TextField()
	descricao = models.TextField()
	data_hora_tweet = models.TextField()
	nome_rotulador = models.TextField()
	#rotuladores = ListField()#EmbeddedModelField('User')
	intervalo = ListField()
	opinioes = ListField() #EmbeddedModelField('Opniao')
	objects = MongoDBManager()

class Opniao(models.Model):
	"""  sim=1, nao=2 """
	labelPositivo = models.IntegerField()
	labelNegativo = models.IntegerField()
	labelNeutro = models.IntegerField()
	labelNaoSeAplica = models.IntegerField()
